# NXtransformations axes

Improve documentation to clarify that *NXtransformations* are active transformations:

https://hdf5.gitlab-pages.esrf.fr/nexus/nxtransformations_active/classes/base_classes/NXtransformations.html

Submitted to the official *NeXus standard*:

https://github.com/nexusformat/definitions/pull/1278
